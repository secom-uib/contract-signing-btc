# contract-signing-btc

**IMPORTANT**: this project is a simple but almost complete Proof-of-concept application.

Project contains all Java classes to execute a complete contract signing protocol flow with Bitcoin Testnet3 support to store non-repudiation evidences. 

We provide within the repository all data from an example execution (the same presented in submitted paper):

- pub/priv ECC keys for Alice and Bob
- contract (PDF) to be signed during exchange (Bitcoin original paper)
- jar file to execute protocol flow (protocol-flow-execution.jar)
- jar file to execute local verification (protocol-verification-local.jar)
- jar file to execute verification calling to Blockcypher online API (protocol-verification-blockcypher.jar)



## Story of test example (with local verification)

1. Send test bitcoins from a Faucet

[Test bitcoins sent from a Bitcoin Testnet3 Faucet](figures/tx-from-faucet.png)


2. Open Blockcypher website to show address balance and wait a single confirmation at least

https://live.blockcypher.com/btc-testnet/address/mirpNr9ZpT58A91eYZc3DnamNWPZXgZW8v/


3. Execute the following command in the folder where protocol-flow-execution.jar is stored:

```
java -jar protocol-flow-execution.jar base-folder deadline-timestamp
```

Where we put *base-folder* as our local folder and *deadline-timestamp* as 1546160400000 (Sunday, December 30, 2018 10:00:00 AM GMT)


4. Wait a moment (few seconds or several minutes depending on your bandwidth and network availability) while blockchain is synchronized (only a small part thanks to the use of SPV mode).

5. After a moment, a Bitcoin trasactions with OP_RETURN is committed to the network. Script creates a new folder bellow *base-folder/tx/tx-hash*, being identifier (hash) of the current example transaction *219e731f4d110a7d87542516a060550f7d774a2a85793df6bbf0a4062696222b*.

6. We have stored in *base-folder/tx/219e731f4d110a7d87542516a060550f7d774a2a85793df6bbf0a4062696222b* three different files:

- nro.dat. It contains the computed NRO (non-repudiation of origin evidence)
- nrr.dat. It contains the computed NRR (non-repudiation of receipt evidence)
- parameters.txt. It contains some parameters computed during protocol execution:
    - first line: contract hash (SHA-256)
    - second line: selected timestamp
    - third line: Alice address
    - fourth line: OP_RETURN as stored into the transactions just committed to the network

7. Blockchyper explorer provide all transaction details:

https://live.blockcypher.com/btc-testnet/tx/219e731f4d110a7d87542516a060550f7d774a2a85793df6bbf0a4062696222b/

8. Verification can be whether local or online with the support of Blockcypher API.

9. (Local verification) Once a transaction is created and committed to the network, a verification process can be executed to verify correctness of all data exchanged during protocol flow and those data published to the blockchain as OP_RETURN content. To do that, execute the jar file *protocol-verification-local.jar* as following:

```
java -jar protocol-verification-local.jar base-folder alice-address deadline-timestamp tx-identifier op-return-content-hex
```

Each parameter is self-explanatory. For the example, we call jar file with the following parameters

```
java -jar protocol-verification-local.jar base-folder mirpNr9ZpT58A91eYZc3DnamNWPZXgZW8v  1546160400000 219e731f4d110a7d87542516a060550f7d774a2a85793df6bbf0a4062696222b 8c8f5c161798085bcda37c10ea861c5622817189d16e141b5e8e8b51ee103713b694022a0aa22aed58bdaaf9b9b017171535fb4b720abaf592c78bbb56e81e6c
```

10. (Local verification) Script returns validation results:

```
[user@host contract-signing-btc]$ java -jar protocol-verification-local.jar base-folder mirpNr9ZpT58A91eYZc3DnamNWPZXgZW8v  1546160400000 219e731f4d110a7d87542516a060550f7d774a2a85793df6bbf0a4062696222b 8c8f5c161798085bcda37c10ea861c5622817189d16e141b5e8e8b51ee103713b694022a0aa22aed58bdaaf9b9b017171535fb4b720abaf592c78bbb56e81e6c
Provided parameters: 
	Base folder: [base-folder]/contract-signing-btc
	Alice address: mirpNr9ZpT58A91eYZc3DnamNWPZXgZW8v
	Deadline timestamp: 1546160400000
	OP_RETURN content HEX: 8c8f5c161798085bcda37c10ea861c5622817189d16e141b5e8e8b51ee103713b694022a0aa22aed58bdaaf9b9b017171535fb4b720abaf592c78bbb56e81e6c
	Document to sign: [base-folder]/contract.pdf

NRO verification result true

NRR verification result true

First part of OP_RETURN content
	As published in blockchain: 8c8f5c161798085bcda37c10ea861c5622817189d16e141b5e8e8b51ee103713
	As rebuilt from stored data:8c8f5c161798085bcda37c10ea861c5622817189d16e141b5e8e8b51ee103713
	Equal? true

Second part of OP_RETURN content
	As published in blockchain: b694022a0aa22aed58bdaaf9b9b017171535fb4b720abaf592c78bbb56e81e6c
	As rebuilt from stored data:b694022a0aa22aed58bdaaf9b9b017171535fb4b720abaf592c78bbb56e81e6c
	Equal? true

```

The script outputs results:

- NRO verification OK (signature ok)
- NRR verification OK (signature ok)
- First hash included in OP_RETURN is equal to the rebuilt hash from stored data previously computed and exchanged during the protocol execution
- Second hash included in OP_RETURN is equal to the rebuilt hash from stored data previously computed and exchanged during the protocol execution


11. (Online verification) In a similar way, it can be conducted an online verification with the support of Blockcypher online blockchain explorer. To do that, execute the jar file *protocol-verification-blockcypher.jar* as following:

```
java -jar protocol-verification-blockcypher.jar base-folder tx-identifier deadline-timestamp
```

Each parameter is also self-explanatory. For the example, we call jar file with the following parameters:


```
java -jar protocol-verification-blockcypher.jar base-folder 219e731f4d110a7d87542516a060550f7d774a2a85793df6bbf0a4062696222b 1546160400000
```


12. (Online verification) Script returns the following results: 

```
Transaction outputs: 
	Type: null-data	is OP_RETURN? 	true
		OP_RETURN script: 6a408c8f5c161798085bcda37c10ea861c5622817189d16e141b5e8e8b51ee103713b694022a0aa22aed58bdaaf9b9b017171535fb4b720abaf592c78bbb56e81e6c
		OP_RETURN data: 8c8f5c161798085bcda37c10ea861c5622817189d16e141b5e8e8b51ee103713b694022a0aa22aed58bdaaf9b9b017171535fb4b720abaf592c78bbb56e81e6c
	Type: pay-to-pubkey-hash	is OP_RETURN? 	false
Transaction inputs: 1
	Alice address: mirpNr9ZpT58A91eYZc3DnamNWPZXgZW8v
Begin verifications...
	NRO verification result valid
	NRR verification result valid
	OP_RETURN data from blockchain is the same as OP_RETURN data computed
	First part of OP_RETURN data (first hash)
		From blockchain:  8c8f5c161798085bcda37c10ea861c5622817189d16e141b5e8e8b51ee103713
		From computation: 8c8f5c161798085bcda37c10ea861c5622817189d16e141b5e8e8b51ee103713
		Equal? true
	Second part of OP_RETURN data (second hash)
		From blockchain:  b694022a0aa22aed58bdaaf9b9b017171535fb4b720abaf592c78bbb56e81e6c
		From computation: b694022a0aa22aed58bdaaf9b9b017171535fb4b720abaf592c78bbb56e81e6c
		Equal? true
Verification successfully completed
```


So, this verifies that protocol has been executed in the right way and OP_RETURN data has been published OK to the blockchain. 


## Preparation
You need *git* to clone this repository and *maven* to compile and build code. These are the steps to setup this project on your development machine:

1. Clone git repository. Go to a folder where you want to the repository be cloned and run:

```
git clone https://gitlab.com/secom-uib/contract-signing-btc.git 
```

2. Package project. In the project's root folder run:

```
mvn package -DskipTests=true
```

As a result of the package stage, you obtain two different jar files: 
- *protocol-flow-execution.jar*. This jar file points to a class that executes a complete protocol flow, stores some data in subfolders for further inspection and publish data to blockchain as an OP_RETURN transaction. 
- *protocol-verification-local.jar*. This jar file points to a class that upon some input values (specified above) verifies locally (without any other API call to third-party service) all data generated during the protocol flow and the data published as OP_RETURN. 


## First execution 
The first time script *protocol-flow-execution.jar* is executed, there are no funds to publish an OP_RETURN transaction, so the jar file returns the following message (of course, replacing *base-folder* and *deadline-timestamp* for working values): 

```
[user@host contract-signing-btc]$ java -jar protocol-flow-execution.jar base-folder deadline-timestamp
Selected base folder base-folder
Selected deadline: deadline-timestamp
Bootstraping Bitcoin client
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
Wait until blockchain is in sync with network...
Blockchain is already in sync with network!
Current available balance is 0.00 BTC
Address from local Bitcoin wallet BITCOIN_TESTNET_ADDRESS (where to deposit some funds to operate)
List of unspents
Print unspents
java.lang.RuntimeException: Please, send some bucks to BITCOIN_TESTNET_ADDRESS to start to operate with protocol
	at cat.uib.secom.cs.bitcoin.BitcoinTX.bootstrap(BitcoinTX.java:105)
	at cat.uib.secom.cs.bitcoin.BitcoinTX.<init>(BitcoinTX.java:42)
	at cat.uib.secom.cs.blockchain.protocol.ProtocolFlowExecution.main(ProtocolFlowExecution.java:42)

```

For the fist time, it is necessary to send some test bitcoins to the address specified above in BITCOIN_TESTNET_ADDRESS (script returns a fresh new address where to deposit some bitcoins replacing BITCION_TESTNET_ADDRESS string). Any working Testnet3 Faucet can be used, for example: https://coinfaucet.eu/en/btc-testnet/


## Libraries
The following libraries have been used:

- Bitcoinj. Library for working with Bitcoin protocol. 
https://bitcoinj.github.io/
- Blockcypher-java. An unofficial Java client for Blockcypher online API.
https://github.com/mmazi/blockcypher-java
- Bouncycastle. Huge and complete library with many many cryptographic algorithms. 
http://www.bouncycastle.org/

