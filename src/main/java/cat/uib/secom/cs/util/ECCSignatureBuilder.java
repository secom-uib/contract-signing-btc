package cat.uib.secom.cs.util;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;

public class ECCSignatureBuilder {

    /**
     * https://stackoverflow.com/questions/22963581/reading-elliptic-curve-private-key-from-file-with-bouncycastle
     * 
     * @param filePath
     * @return
     * @throws IOException
     */
    public static PrivateKey getECCPrivateKey(String filePath) throws IOException {

        Security.addProvider(new BouncyCastleProvider());

        Reader reader = new FileReader(filePath);

        Object parsed = new PEMParser(reader).readObject();
        KeyPair pair = new JcaPEMKeyConverter().getKeyPair((PEMKeyPair) parsed);
        return pair.getPrivate();
    }

    /**
     * https://stackoverflow.com/questions/40434317/how-to-load-pem-encoded-elliptic-curve-public-keys-into-bouncy-castle
     * 
     * @param filePath
     * @return
     * @throws IOException
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public static PublicKey getECCPublicKey(String filePath)
            throws IOException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException {

        Security.addProvider(new BouncyCastleProvider());
        Reader reader = new FileReader(filePath);
        PemObject spki = new PemReader(reader).readPemObject();
        PublicKey pubKey = KeyFactory.getInstance("EC", "BC").generatePublic(new X509EncodedKeySpec(spki.getContent()));
        return pubKey;
    }

    public static byte[] sign(byte[] data, PrivateKey privKey, String algorithm)
            throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException {

        Security.addProvider(new BouncyCastleProvider());
        Signature sign = Signature.getInstance(algorithm, "BC");
        sign.initSign(privKey);
        sign.update(data);
        byte[] signature = sign.sign();
        return signature;
    }

    public static boolean verify(byte[] data, byte[] signature, PublicKey pubKey, String algorithm)
            throws SignatureException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException {

        Security.addProvider(new BouncyCastleProvider());
        Signature sign = Signature.getInstance(algorithm, "BC");
        sign.initVerify(pubKey);
        sign.update(data);
        return sign.verify(signature);

    }
}