package cat.uib.secom.cs.util;

public class DefaultConfiguration {

    public static final String SIGNATURE_ALGORITHM = "SHA256withECDSA";
    public static final String HASH_ALGORITHM = "SHA-256";
    public static final String DEFAULT_PRIVATEKEY_FILENAME = "ecpubkey.pem";
    public static final String DEFAULT_PUBLICKEY_FILENAME = "secp256k1-key.pem";
}