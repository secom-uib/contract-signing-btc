package cat.uib.secom.cs.util;

import java.math.BigInteger;
import java.util.Base64;

public class FormatUtils {

    public enum OPTIONS {
        HEX, BASE64, BIN
    }

    public static String returnOutput(byte[] array, OPTIONS outputFormat) {
        System.out.println("\t\tOriginal byte size=" + array.length);

        if (outputFormat.equals(OPTIONS.HEX)) {
            return toHex(array);
        } else if (outputFormat.equals(OPTIONS.BASE64)) {
            return toBase64(array);
        } else if (outputFormat.equals(OPTIONS.BIN)) {
            return toBinary(array);
        } else {
            throw new RuntimeException();
        }
    }

    public static String toHex(byte[] array) {
        StringBuilder result = new StringBuilder();
        for (byte b : array) {
            result.append(String.format("%02x", b));
        }
        return result.toString();
    }

    public static String toBase64(byte[] array) {
        byte[] encoded = Base64.getEncoder().encode(array);
        return new String(encoded);
    }

    public static String toBinary(byte[] array) {
        BigInteger bi = new BigInteger(array);
        return bi.toString(2);
    }

    public static void prettyPrint(byte[] data, String variableName) {
        System.out.println(variableName + " codification and lengths: " + "\n\tRaw data: \t\t(" + data.length
                + "bytes) \t" + new String(data) + "\n\tHex encoded data: \t(" + FormatUtils.toHex(data).length()
                + "bytes) \t" + FormatUtils.toHex(data) + "\n\tBase64 encoded data: \t("
                + FormatUtils.toBase64(data).length() + "bytes) \t" + FormatUtils.toBase64(data));

    }

}