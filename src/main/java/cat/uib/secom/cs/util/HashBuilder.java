package cat.uib.secom.cs.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;

public class HashBuilder {

    public static byte[] hash(byte[] data, MessageDigest md) {
        return md.digest(data);
    }

    public static byte[] hash(String filepath, MessageDigest md) throws Exception {

        try {
            InputStream is = new FileInputStream(filepath);

            DigestInputStream dis = new DigestInputStream(is, md);
            while (dis.read() != -1) {
                md = dis.getMessageDigest();
            }

            dis.close();

            return md.digest();

        } catch (Exception e) {
            throw e;
        }
    }

    public static byte[] hashStringData(String data, MessageDigest md) throws Exception {

        return md.digest(data.getBytes());
    }

}