package cat.uib.secom.cs.bitcoin;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.concurrent.ExecutionException;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.InsufficientMoneyException;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.TransactionConfidence;
import org.bitcoinj.core.TransactionOutput;
import org.bitcoinj.crypto.KeyCrypterException;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;
import org.bitcoinj.wallet.SendRequest;
import org.bitcoinj.wallet.Wallet;
import org.bitcoinj.wallet.Wallet.SendResult;
import org.bitcoinj.wallet.listeners.WalletCoinsSentEventListener;

public class BitcoinTX {

    private WalletAppKit kit;
    private NetworkParameters params;
    private String workingFolderPath;

    // address to where to deposit some funds to operate
    private Address localAddress;
    // address to operate with (single use)
    private Address operationAddress;

    public BitcoinTX(String workingFolderPath) {
        this.workingFolderPath = workingFolderPath;
        bootstrap();
    }

    public Address getLocalAddress() {
        return this.localAddress;
    }

    public Address getOperationAddress() {
        return this.operationAddress;
    }

    public void setOperationAddress(String address) {
        this.operationAddress = Address.fromBase58(params, address);
    }

    protected void bootstrap() {

        System.out.println("Bootstraping Bitcoin client");
        params = TestNet3Params.get();
        String filePrefix = "contract-signature-testnet";

        // Start up a basic app using a class that automates some boilerplate.
        kit = new WalletAppKit(params, new File(this.workingFolderPath), filePrefix) {
            @Override
            protected void onSetupCompleted() {
                if (wallet().getKeyChainGroupSize() < 1) {
                    wallet().importKey(new ECKey());
                }
            }
        };

        System.out.println("Wait until blockchain is in sync with network...");
        // Download the block chain and wait until it's done.
        // System.out.println(kit.state());

        kit.startAsync();
        kit.awaitRunning();

        this.localAddress = kit.wallet().currentReceiveKey().toAddress(params);
        System.out.println("Blockchain is already in sync with network!");
        System.out.println("Current available balance is " + kit.wallet().getBalance().toFriendlyString());
        System.out.println("Address from local Bitcoin wallet " + localAddress.toBase58()
                + " (where to deposit some funds to operate)");

        // System.out.println(kit.wallet().currentAddress(KeyPurpose.RECEIVE_FUNDS));
        // System.out.println(kit.wallet().currentAddress(KeyPurpose.AUTHENTICATION));
        // System.out.println(kit.wallet().currentAddress(KeyPurpose.CHANGE));
        // System.out.println(kit.wallet().currentAddress(KeyPurpose.REFUND));
        System.out.println("List of unspents");
        for (TransactionOutput to : kit.wallet().getUnspents()) {
            System.out.println("Transaction " + to.getIndex());
            System.out.println("\t" + to.getAddressFromP2PKHScript(params));
            System.out.println("\t" + to.getAddressFromP2SH(params));
            System.out.println("\t" + to.getParentTransaction().getHashAsString());
        }

        System.out.println("Print unspents");
        for (TransactionOutput to : kit.wallet().getUnspents()) {
            System.out.println("\t" + to);
        }
        if (kit.wallet().getUnspents().size() > 0) {
            this.operationAddress = kit.wallet().getUnspents().get(0).getAddressFromP2PKHScript(params);
        } else {
            throw new RuntimeException(
                    "Please, send some bucks to " + localAddress.toBase58() + " to start to operate with protocol");
        }

    }

    public Transaction createOpReturnTx(byte[] opReturnContent) throws UnsupportedEncodingException {
        try {
            Wallet wallet = kit.wallet();

            System.out.println("\n\nTrying to create an OP_RETURN Bitcoin transaction");

            if (wallet.getBalance().isZero()) {
                System.out.println(
                        "There are no funds to create a new transaction until recent transaction will be confirmed by blockchain network");
                System.out.println("Please! Wait some blocks to create a new transaction");
                return null;
            } else {

                System.out.println("Creation of OpReturn TX begins");
                // Create new Bitcoin transaction
                Transaction newTx = new Transaction(this.params);
                // Create new OP_RETURN script
                Script script = ScriptBuilder.createOpReturnScript(opReturnContent);
                // Add OP_RETURN as output with zero value

                newTx.addOutput(Coin.ZERO, script);

                SendRequest request = SendRequest.forTx(newTx);
                // wallet.completeTx(request);request
                // Commit to network
                // wallet.commitTx(request.tx);
                System.out.println("before sendCoins method");
                SendResult sendResult = wallet.sendCoins(request);
                System.out.println("before get() method");
                sendResult.broadcastComplete.get();

                // PeerGroup peerGroup = new PeerGroup(params);
                // peerGroup.broadcastTransaction(request.tx); //.future().get();

                System.out.println("\n\n*** OpReturn TX committed to network ***");

                System.out.println("Sending TX (" + newTx.getHash() + ") with outputSum="
                        + newTx.getOutputSum().toFriendlyString() + " and fee=" + newTx.getFee().toFriendlyString());
                // Show TX data with friendly format
                System.out.println("Transaction data\n " + newTx);

                // Get pending transactions
                System.out.println("List of pending transactions: " + kit.wallet().getPendingTransactions());
                return newTx;
            }

        } catch (KeyCrypterException | InsufficientMoneyException | InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public Collection<Transaction> getPendingTransactions() {
        return kit.wallet().getPendingTransactions();
    }

    public Collection<Transaction> getRecentTransactions() {
        return kit.wallet().getRecentTransactions(10, true);
    }

    public void prettyPrintPendingTrasactions() {
        System.out.println("\n/////////////// Checking whether there are PENDING transactions ////////////");
        Collection<Transaction> pendingTxs = this.getPendingTransactions();
        if (pendingTxs.size() == 0) {
            System.out.println("\tNo pending transactions");
        } else {
            System.out.println("\tThere are some pending transactions (" + pendingTxs.size() + ")");
            int i = 1;
            for (Transaction tx : pendingTxs) {
                System.out.println("\t\tTx" + i++);
                System.out.println("\t\t" + tx.getUpdateTime());
                System.out.println("\t\tHash=" + tx.getHashAsString() + " confirmations="
                        + tx.getConfidence().getDepthInBlocks() + " blocks");
                for (TransactionOutput tout : tx.getOutputs()) {
                    if (tout.getScriptPubKey().isOpReturn()) {
                        System.out.println("\t\tOP_RETURN content: " + tout.getScriptPubKey());
                    }
                }
            }
        }
    }

    public void prettyPrintRecentTransactions() {
        System.out.println("\n///////////// Checking whether there are RECENT transactions ///////////////");
        Collection<Transaction> recentTxs = this.getRecentTransactions();
        if (recentTxs.size() == 0) {
            System.out.println("\tNo recent transactions");
        } else {
            System.out.println("\tThere are some recent transactions (" + recentTxs.size() + ")");
            int i = 1;
            for (Transaction tx : recentTxs) {
                System.out.println("\t\tTx" + i++);
                System.out.println("\t\t" + tx.getUpdateTime());
                System.out.println("\t\tHash=" + tx.getHashAsString() + " depth="
                        + tx.getConfidence().getDepthInBlocks() + " blocks");
                for (TransactionOutput tout : tx.getOutputs()) {
                    if (tout.getScriptPubKey().isOpReturn()) {
                        System.out.println("\t\tOP_RETURN content: " + tout.getScriptPubKey());
                    }
                }
            }
        }
    }

    public boolean hasFunds() {
        if (kit.wallet().getBalance().isZero()) {
            return false;
        }
        return true;
    }

    public void addCallback(Transaction tx, int depth) {

        // tx.getConfidence().addEventListener((confidence, reason) -> {

        // });

        System.out.println(
                "Add callback to wait for a confirmation to " + tx.getHashAsString() + " (" + depth + " blocks)");
        Futures.addCallback(tx.getConfidence().getDepthFuture(depth), new FutureCallback<TransactionConfidence>() {
            @Override
            public void onSuccess(TransactionConfidence result) {
                System.out.println("*** Confirmation received to tx " + tx.getHashAsString() + " ***");
            }

            @Override
            public void onFailure(Throwable t) {
                // This kind of future can't fail, just rethrow in case something weird happens.
                throw new RuntimeException(t);
            }
        });
    }

    public void addCoinSendListener() {
        kit.wallet().addCoinsSentEventListener(new WalletCoinsSentEventListener() {
            @Override
            public void onCoinsSent(Wallet wallet, Transaction tx, Coin prevBalance, Coin newBalance) {
                System.out.println("coins sent " + tx.getHashAsString());
            }
        });
    }

}