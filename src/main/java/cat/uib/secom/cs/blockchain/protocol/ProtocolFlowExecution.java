package cat.uib.secom.cs.blockchain.protocol;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.security.MessageDigest;

import com.google.common.base.Charsets;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.Transaction;

import cat.uib.secom.cs.bitcoin.BitcoinTX;
import cat.uib.secom.cs.blockchain.messages.Message1;
import cat.uib.secom.cs.blockchain.messages.Message2;
import cat.uib.secom.cs.blockchain.messages.Message3;
import cat.uib.secom.cs.blockchain.parties.Alice;
import cat.uib.secom.cs.blockchain.parties.Bob;
import cat.uib.secom.cs.util.DefaultConfiguration;
import cat.uib.secom.cs.util.FormatUtils;
import cat.uib.secom.cs.util.HashBuilder;

public class ProtocolFlowExecution {

    public static void main(String[] args) {
        try {

            if (args.length < 2) {
                System.err.println("Usage: base-folder deadline-timestamp");
                return;
            }

            String baseFolder = args[0];
            String inputDeadline = args[1];

            System.out.println("Selected base folder " + baseFolder);
            System.out.println("Selected deadline: " + inputDeadline);

            // create Bitcoin client
            BitcoinTX aliceBitcoinClient = new BitcoinTX(baseFolder + "/sign-contract-btc-poc/");
            Address aliceAddress = aliceBitcoinClient.getLocalAddress();
            System.out.println("***** Alice's Bitcoin address: " + aliceAddress.toBase58() + " *****");

            // override operation address with an address I control
            // aliceBitcoinClient.setOperationAddress("mwSboSKSjyZyiEG9PMbaQCBbeZdtEbsqMX");
            System.out.println("***** Alice's operation address: " + aliceBitcoinClient.getOperationAddress().toBase58()
                    + " *****");

            aliceBitcoinClient.prettyPrintPendingTrasactions();
            aliceBitcoinClient.prettyPrintRecentTransactions();

            if (!aliceBitcoinClient.hasFunds()) {
                System.out.println("No available funds to create a new transaction");
            } else {

                long init = System.currentTimeMillis();
                Alice alice = new Alice(baseFolder + "/secp256k1-alice-key.pem");
                // TODO: different keys for Alice and Bob
                Bob bob = new Bob(baseFolder + "/secp256k1-bob-key.pem");

                // contract hash field
                String contractFilepath = baseFolder + "/contract.pdf";
                byte[] contract = HashBuilder.hash(contractFilepath,
                        MessageDigest.getInstance(DefaultConfiguration.HASH_ALGORITHM));

                // timestamp field
                // Calendar calendar = new GregorianCalendar(year,month,day,hour,minute,second);
                long timestamp = Long.parseLong(inputDeadline); // calendar.getTimeInMillis(); //
                                                                // Instant.now().toEpochMilli();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(baos);
                dos.writeLong(timestamp);
                dos.close();
                byte[] deadline = baos.toByteArray();

                // Alice' address field
                String addr = aliceBitcoinClient.getOperationAddress().toBase58();
                byte[] addrAlice = addr.getBytes(Charsets.UTF_8);

                Message1 message1 = alice.generateMessage1(contract, deadline, addrAlice);
                byte[] nro = message1.getNro(); // store to file

                bob.receiveMessage1(message1);

                Message2 message2 = bob.generateMessage2();
                byte[] nrr = message2.getNrr(); // store to file

                alice.receiveMessage2(message2);
                Message3 message3 = alice.generateMessage3();

                long end = System.currentTimeMillis();

                System.out.println("Data to publish as OP_RETURN:");
                message3.prettyPrint();

                String opReturnData = new String(message3.getBytes());
                String opReturnDataHexEncoded = FormatUtils.toHex(message3.getBytes());

                System.out.println("OP_RETURN data as RAW byte[]: \n\t" + opReturnData);
                System.out.println("OP_RETURN data encoded as HEX: \n\t" + opReturnDataHexEncoded);

                System.out.println("Protocol executed in " + (end - init) + " ms");

                // create OP_RETURN TX to blockchain (Bitcoin)

                Transaction tx = aliceBitcoinClient.createOpReturnTx(message3.getBytes());
                String txHash = tx.getHashAsString();
                // String txHash = "test";

                // store NRO and NRR to file
                String dataFilepath = baseFolder + "/tx/" + txHash;
                // test if this folder exists and creates otherwise
                File folder = new File(dataFilepath);
                if (!folder.exists()) {
                    System.out.println("Creating folder for tx " + txHash + "(" + dataFilepath + ")");
                    folder.mkdirs();
                }
                String nroFilepath = dataFilepath + "/nro.dat";
                String nrrFilepath = dataFilepath + "/nrr.dat";
                String parametersFilepath = dataFilepath + "/parameters.txt";

                try (FileOutputStream fos = new FileOutputStream(nroFilepath)) {
                    fos.write(nro);
                }
                System.out.println("nro file written for tx " + txHash);
                try (FileOutputStream fos = new FileOutputStream(nrrFilepath)) {
                    fos.write(nrr);
                }
                System.out.println("nrr file written for tx " + txHash);

                try (PrintWriter pw = new PrintWriter(parametersFilepath)) {
                    pw.println(FormatUtils.toHex(contract)); // hash of contract file in HEX
                    pw.println(timestamp); // timestamp in long
                    pw.println(addr); // Alice address
                    pw.println(opReturnDataHexEncoded); // OP_RETURN
                }
                System.out.println("parameters file written for tx " + txHash);
                // closed automatically after completio
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}