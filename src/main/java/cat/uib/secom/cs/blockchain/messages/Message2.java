package cat.uib.secom.cs.blockchain.messages;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SignatureException;

import cat.uib.secom.cs.util.DefaultConfiguration;
import cat.uib.secom.cs.util.ECCSignatureBuilder;
import lombok.Data;

@Data
public class Message2 {

    private byte[] nrr;

    public Message2() {
    }

    public void generateNRR(String privKeyFilepath, Message1 message1) throws IOException, InvalidKeyException,
            NoSuchAlgorithmException, NoSuchProviderException, SignatureException {
        byte[] contract = message1.getContract();
        byte[] deadline = message1.getDeadline();
        byte[] addrAlice = message1.getAddrAlice();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write(contract);
        baos.write(deadline);
        baos.write(addrAlice);
        PrivateKey privKey = ECCSignatureBuilder.getECCPrivateKey(privKeyFilepath);
        nrr = ECCSignatureBuilder.sign(baos.toByteArray(), privKey, DefaultConfiguration.SIGNATURE_ALGORITHM);
    }

    public byte[] getBytes() {
        if (nrr != null) {
            return nrr;
        } else {
            throw new RuntimeException("NRR has to be generated before");
        }
    }

}