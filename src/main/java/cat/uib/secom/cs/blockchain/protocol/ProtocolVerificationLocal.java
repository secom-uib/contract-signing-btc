package cat.uib.secom.cs.blockchain.protocol;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.util.Arrays;

import com.google.common.base.Charsets;

import org.bouncycastle.util.encoders.Hex;

import cat.uib.secom.cs.util.DefaultConfiguration;
import cat.uib.secom.cs.util.ECCSignatureBuilder;
import cat.uib.secom.cs.util.FormatUtils;
import cat.uib.secom.cs.util.HashBuilder;

public class ProtocolVerificationLocal {

    public static void main(String[] args) {

        if (args.length < 5) {
            System.err
                    .println("Usage: base-folder alice-address deadline-timestamp tx-identifier op-return-content-hex");
            return;
        }

        String baseFolder = args[0];

        try {
            // Alice address
            String addrA = args[1];
            // Deadline as a timestamp
            long timestamp = Long.parseLong(args[2]);
            // TX hash
            String txHash = args[3];
            // OP_RETURN hexadecimal string
            String opReturn = args[4];
            // Document to sign
            String filepath = baseFolder + "/contract.pdf";

            System.out.println("Provided parameters: ");
            System.out.println("\tBase folder: " + baseFolder);
            System.out.println("\tAlice address: " + addrA);
            System.out.println("\tDeadline timestamp: " + timestamp);
            System.out.println("\tOP_RETURN content HEX: " + opReturn);
            System.out.println("\tDocument to sign: " + filepath);

            // sigA(x)
            // read from file
            String nroFilepath = baseFolder + "/tx/" + txHash + "/nro.dat";
            byte[] nro = Files.readAllBytes((new File(nroFilepath).toPath()));

            // sigB(x)
            // read from file
            String nrrFilepath = baseFolder + "/tx/" + txHash + "/nrr.dat";
            byte[] nrr = Files.readAllBytes(new File(nrrFilepath).toPath());

            // Contract - compute hash
            byte[] m = HashBuilder.hash(filepath, MessageDigest.getInstance(DefaultConfiguration.HASH_ALGORITHM));

            // timestamp to byte array
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(baos);
            dos.writeLong(timestamp);
            dos.close();
            byte[] deadline = baos.toByteArray();

            // Address to byte array
            byte[] addrAlice = addrA.getBytes(Charsets.UTF_8);

            // OP_RETURN content decodification from hexadecimal to raw byte array
            byte[] opReturnByte = Hex.decode(opReturn);

            // decode OP_RETURN
            // 32 first bytes are the first part of data [h(m1)]
            byte[] hm1 = new byte[32];
            ByteArrayInputStream bais = new ByteArrayInputStream(opReturnByte);
            bais.read(hm1, 0, 32);
            String hm1Hex = FormatUtils.toHex(hm1);
            // 32 following bytes are the second part of data [AE]
            byte[] ae = new byte[32];
            ae = Arrays.copyOfRange(opReturnByte, 32, opReturnByte.length);
            String aeHex = FormatUtils.toHex(ae);

            baos = new ByteArrayOutputStream();
            baos.write(m);
            baos.write(deadline);
            baos.write(addrAlice);
            byte[] x = baos.toByteArray();

            // Alice pubkey
            String alicePubKeyFilepath = baseFolder + "/ecpubkey-alice.pem";
            PublicKey alicePubKey = ECCSignatureBuilder.getECCPublicKey(alicePubKeyFilepath);
            boolean nroVerification = ECCSignatureBuilder.verify(x, nro, alicePubKey,
                    DefaultConfiguration.SIGNATURE_ALGORITHM);
            System.out.println("\nNRO verification result " + nroVerification);

            // Bob pubkey
            String bobPubKeyFilepath = baseFolder + "/ecpubkey-bob.pem";
            PublicKey bobPubKey = ECCSignatureBuilder.getECCPublicKey(bobPubKeyFilepath);
            boolean nrrVerification = ECCSignatureBuilder.verify(x, nrr, bobPubKey,
                    DefaultConfiguration.SIGNATURE_ALGORITHM);
            System.out.println("\nNRR verification result " + nrrVerification);

            // first hash verification
            baos = new ByteArrayOutputStream();
            baos.write(x);
            baos.write(nro);
            byte[] firstPart = baos.toByteArray();
            // do hash over firstPart
            byte[] hashFirstPart = HashBuilder.hash(firstPart,
                    MessageDigest.getInstance(DefaultConfiguration.HASH_ALGORITHM));
            String hashFirstPartHex = FormatUtils.toHex(hashFirstPart);

            // second hash verification
            baos = new ByteArrayOutputStream();
            baos.write(nro);
            baos.write(nrr);
            byte[] secondPart = baos.toByteArray();
            // do hash over second part
            byte[] hashSecondPart = HashBuilder.hash(secondPart,
                    MessageDigest.getInstance(DefaultConfiguration.HASH_ALGORITHM));
            String hashSecondPartHex = FormatUtils.toHex(hashSecondPart);

            System.out.println("\nFirst part of OP_RETURN content");
            System.out.println("\tAs published in blockchain: " + hm1Hex);
            System.out.println("\tAs rebuilt from stored data:" + hashFirstPartHex);
            System.out.println("\tEqual? " + hm1Hex.equals(hashFirstPartHex));

            System.out.println("\nSecond part of OP_RETURN content");
            System.out.println("\tAs published in blockchain: " + aeHex);
            System.out.println("\tAs rebuilt from stored data:" + hashSecondPartHex);
            System.out.println("\tEqual? " + aeHex.equals(hashSecondPartHex));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}