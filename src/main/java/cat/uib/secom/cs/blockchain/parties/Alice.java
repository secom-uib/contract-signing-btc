package cat.uib.secom.cs.blockchain.parties;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;

import cat.uib.secom.cs.blockchain.messages.Message1;
import cat.uib.secom.cs.blockchain.messages.Message2;
import cat.uib.secom.cs.blockchain.messages.Message3;
import cat.uib.secom.cs.util.DefaultConfiguration;
import cat.uib.secom.cs.util.HashBuilder;
import lombok.Data;

@Data
public class Alice {

    private Message1 message1;
    private Message2 message2;
    private Message3 message3;

    private String privateKeyFilepath;

    public Alice(String privateKeyFilepath) {
        this.privateKeyFilepath = privateKeyFilepath;
    }

    /**
     * Upon contract, deadline and address of Alice, generate first protocol
     * message: - signs data - composes message - returns message
     * 
     * @param contract contract to be signed, previously agreed between Alice and
     *                 Bob
     * @param deadline temporal deadline
     * @param addressA blockchain address where Alice has some funds
     * @return first message to be send from Alice to Bob
     * @throws IOException
     * @throws SignatureException
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public Message1 generateMessage1(byte[] contract, byte[] deadline, byte[] addressA) throws InvalidKeyException,
            NoSuchAlgorithmException, NoSuchProviderException, SignatureException, IOException {

        message1 = new Message1(contract, deadline, addressA);
        message1.generateNRO(privateKeyFilepath);
        byte[] m1 = message1.getBytes();
        return message1;
    }

    /**
     * Receives second message
     * 
     * @param message2
     */
    public void receiveMessage2(Message2 message2) {
        this.message2 = message2;
        // verify signature of message2
    }

    /**
     * Upon data previously generated and data received from Bob, Alice generates
     * thrid message, which has to be send to blockchain
     * 
     * @return third message to be send from Alice to blockchain
     * @throws IOException
     * @throws SignatureException
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public Message3 generateMessage3() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException,
            SignatureException, IOException {

        byte[] hm1 = HashBuilder.hash(message1.getBytes(),
                MessageDigest.getInstance(DefaultConfiguration.HASH_ALGORITHM));
        byte[] sigA = message1.getNro();
        byte[] sigB = message2.getNrr();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write(sigA);
        baos.write(sigB);
        byte[] hnronrr = HashBuilder.hash(baos.toByteArray(),
                MessageDigest.getInstance(DefaultConfiguration.HASH_ALGORITHM));
        message3 = new Message3(hm1, hnronrr);
        byte[] m3 = message3.getBytes();
        return message3;
    }

}