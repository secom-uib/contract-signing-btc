package cat.uib.secom.cs.blockchain.parties;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;

import cat.uib.secom.cs.blockchain.messages.Message1;
import cat.uib.secom.cs.blockchain.messages.Message2;
import cat.uib.secom.cs.blockchain.messages.Message3;
import lombok.Data;

@Data
public class Bob {

    private Message1 message1;
    private Message2 message2;
    private Message3 message3;

    private String privateKeyFilepath;

    public Bob(String privateKeyFilepath) {
        this.privateKeyFilepath = privateKeyFilepath;
    }

    public void receiveMessage1(Message1 message1) {
        this.message1 = message1;
    }

    public Message2 generateMessage2() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException,
            SignatureException, IOException {
        message2 = new Message2();
        message2.generateNRR(privateKeyFilepath, message1);
        byte[] m2 = message2.getBytes();
        return message2;
    }

}