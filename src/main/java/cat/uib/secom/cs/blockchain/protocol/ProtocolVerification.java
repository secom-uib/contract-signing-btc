package cat.uib.secom.cs.blockchain.protocol;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.util.Arrays;

import com.github.mmazi.blockcypher.BlockCypher;
import com.github.mmazi.blockcypher.data.Input;
import com.github.mmazi.blockcypher.data.Output;
import com.github.mmazi.blockcypher.data.Transaction;
import com.google.common.base.Charsets;

import org.bouncycastle.util.encoders.Hex;

import cat.uib.secom.cs.util.DefaultConfiguration;
import cat.uib.secom.cs.util.ECCSignatureBuilder;
import cat.uib.secom.cs.util.FormatUtils;
import cat.uib.secom.cs.util.HashBuilder;
import si.mazi.rescu.RestProxyFactory;

public class ProtocolVerification {

    public static final String BLOCKCYPHER_API = "https://api.blockcypher.com/v1/btc/test3";

    public static void main(String[] args) {

        if (args.length < 3) {
            System.err.println("Usage: base-folder tx-hash deadline-timestamp");
            return;
        }

        // base folder
        String baseFolderParameter = args[0];
        // TX hash
        String txHashParameter = args[1];
        // deadline
        String deadlineParameter = args[2];

        try {
            // get info from blockcypher API about txHash
            BlockCypher bc = RestProxyFactory.createProxy(BlockCypher.class, BLOCKCYPHER_API);
            // API call without authentication (it is a GET call)
            Transaction transaction = bc.getTransaction(txHashParameter, false, "");
            // Check if it has an OP_RETURN output

            String opReturnFromBlockchain = "";
            System.out.println("Transaction outputs: ");
            for (Output out : transaction.getOutputs()) {
                System.out.println("\tType: " + out.getScriptType() + "\tis OP_RETURN? \t"
                        + out.getScriptType().equals("null-data"));
                if (out.getScriptType().equals("null-data")) {
                    System.out.println("\t\tOP_RETURN script: " + out.getScript());
                    // OP_RETURN data, without beginning bytes (used library outputs script encoded
                    // as hexadecimal)
                    opReturnFromBlockchain = out.getScript().substring(4, out.getScript().length());
                    System.out.println("\t\tOP_RETURN data: " + opReturnFromBlockchain);
                }
            }

            String addrA = "";
            // POC only distinguishes a transaction with a single input: the input Alice
            // used to make OP_RETURN transaction
            System.out.println("Transaction inputs: " + transaction.getInputs().length);
            if (transaction.getInputs().length == 0) {
                throw new RuntimeException("No inputs detected in selected transaction");
            } else if (transaction.getInputs().length > 1) {
                throw new RuntimeException("POC does not support more than single input");
            } else {
                Input input = transaction.getInputs()[0];
                if (input.getAddresses().length == 1) {
                    addrA = input.getAddresses()[0];
                    System.out.println("\tAlice address: " + addrA);
                } else {
                    throw new RuntimeException("POC does not support more than single input addresses");
                }
            }

            System.out.println("\nBegin verifications...");

            // decode OP_RETURN as published in blockchain (raw byte array)
            byte[] opReturnByte = Hex.decode(opReturnFromBlockchain);
            // 32 first bytes are the first part of data [h(m1)]
            byte[] hm1 = new byte[32];
            ByteArrayInputStream bais = new ByteArrayInputStream(opReturnByte);
            bais.read(hm1, 0, 32);
            String hm1Hex = FormatUtils.toHex(hm1);
            // 32 following bytes are the second part of data [AE]
            byte[] ae = new byte[32];
            ae = Arrays.copyOfRange(opReturnByte, 32, opReturnByte.length);
            String aeHex = FormatUtils.toHex(ae);

            // Rebuild OP_RETURN to compare with published one
            // Document to sign
            String filepath = baseFolderParameter + "/contract.pdf";

            // sigA(x)
            // read from file
            String nroFilepath = baseFolderParameter + "/tx/" + txHashParameter + "/nro.dat";
            byte[] nro = Files.readAllBytes((new File(nroFilepath).toPath()));

            // sigB(x)
            // read from file
            String nrrFilepath = baseFolderParameter + "/tx/" + txHashParameter + "/nrr.dat";
            byte[] nrr = Files.readAllBytes(new File(nrrFilepath).toPath());

            // Contract
            byte[] m = HashBuilder.hash(filepath, MessageDigest.getInstance(DefaultConfiguration.HASH_ALGORITHM));

            // timestamp to byte array
            long timestamp = Long.parseLong(deadlineParameter);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(baos);
            dos.writeLong(timestamp);
            dos.close();
            byte[] deadline = baos.toByteArray();

            // Address to byte array
            byte[] addrAlice = addrA.getBytes(Charsets.UTF_8);

            // Rebuild data signed by Alice: (M, t_d, Adr_A)
            baos = new ByteArrayOutputStream();
            baos.write(m);
            baos.write(deadline);
            baos.write(addrAlice);
            byte[] x = baos.toByteArray();

            // Alice pubkey
            String alicePubKeyFilepath = baseFolderParameter + "/ecpubkey-alice.pem";
            PublicKey alicePubKey = ECCSignatureBuilder.getECCPublicKey(alicePubKeyFilepath);
            // NRO verification with pubkey
            boolean nroVerification = ECCSignatureBuilder.verify(x, nro, alicePubKey,
                    DefaultConfiguration.SIGNATURE_ALGORITHM);
            System.out.println("\tNRO verification result " + (nroVerification ? "valid" : "invalid"));

            // Bob pubkey
            String bobPubKeyFilepath = baseFolderParameter + "/ecpubkey-bob.pem";
            PublicKey bobPubKey = ECCSignatureBuilder.getECCPublicKey(bobPubKeyFilepath);
            // NRR verification with pubkey
            boolean nrrVerification = ECCSignatureBuilder.verify(x, nrr, bobPubKey,
                    DefaultConfiguration.SIGNATURE_ALGORITHM);
            System.out.println("\tNRR verification result " + (nrrVerification ? "valid" : "invalid"));

            // Rebuild OP_RETURN content locally to check whether is the same as was
            // published by Alice

            // rebuild first hash of OP_RETURN data
            baos = new ByteArrayOutputStream();
            baos.write(x);
            baos.write(nro);
            byte[] firstPart = baos.toByteArray();
            // do hash over firstPart
            byte[] hashFirstPart = HashBuilder.hash(firstPart,
                    MessageDigest.getInstance(DefaultConfiguration.HASH_ALGORITHM));
            String hashFirstPartHex = FormatUtils.toHex(hashFirstPart);

            // rebuild second hash of OP_RETURN data
            baos = new ByteArrayOutputStream();
            baos.write(nro);
            baos.write(nrr);
            byte[] secondPart = baos.toByteArray();
            // do hash over second part
            byte[] hashSecondPart = HashBuilder.hash(secondPart,
                    MessageDigest.getInstance(DefaultConfiguration.HASH_ALGORITHM));
            String hashSecondPartHex = FormatUtils.toHex(hashSecondPart);

            String rebuildOpReturn = hashFirstPartHex + "" + hashSecondPartHex;
            if (opReturnFromBlockchain.equals(rebuildOpReturn)) {
                System.out.println("\tOP_RETURN data from blockchain is the same as OP_RETURN data computed");
            } else {
                throw new RuntimeException("\tVerification of OP_RETURN data published on blockchain failed!");
            }

            System.out.println("\tFirst part of OP_RETURN data (first hash)");
            System.out.println("\t\tFrom blockchain:  " + hm1Hex);
            System.out.println("\t\tFrom computation: " + hashFirstPartHex);
            System.out.println("\t\tEqual? " + hm1Hex.equals(hashFirstPartHex));

            System.out.println("\tSecond part of OP_RETURN data (second hash)");
            System.out.println("\t\tFrom blockchain:  " + aeHex);
            System.out.println("\t\tFrom computation: " + hashSecondPartHex);
            System.out.println("\t\tEqual? " + aeHex.equals(hashSecondPartHex));

            System.out.println("Verification successfully completed");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void prettyPrint(String[] array) {
        for (String s : array) {
            System.out.println(s);
        }
    }

}