package cat.uib.secom.cs.blockchain.messages;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import cat.uib.secom.cs.util.FormatUtils;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Message3 {

    private byte[] hm1 = null;
    private byte[] hnronrr = null;

    public byte[] getBytes() throws IOException {
        if (hm1 != null && hnronrr != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.write(hm1);
            baos.write(hnronrr);
            return baos.toByteArray();
        } else {
            throw new RuntimeException("Fields should be filled before calling method");
        }
    }

    public void prettyPrint() throws IOException {
        if (hm1 != null && hnronrr != null) {
            FormatUtils.prettyPrint(getBytes(), "m3=[h(m1),h(NRO,NRR)]");
        } else {
            throw new RuntimeException("Fields should be filled before calling method");
        }
    }

}