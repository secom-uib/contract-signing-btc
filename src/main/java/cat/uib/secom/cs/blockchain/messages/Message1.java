package cat.uib.secom.cs.blockchain.messages;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SignatureException;

import cat.uib.secom.cs.util.DefaultConfiguration;
import cat.uib.secom.cs.util.ECCSignatureBuilder;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Message1 {

    private byte[] contract = null;
    private byte[] deadline = null;
    private byte[] addrAlice = null;
    private byte[] nro = null;

    public Message1(byte[] contract, byte[] deadline, byte[] addrAlice) {
        this.contract = contract;
        this.deadline = deadline;
        this.addrAlice = addrAlice;
    }

    public void generateNRO(String privKeyFilepath) throws IOException, InvalidKeyException, NoSuchAlgorithmException,
            NoSuchProviderException, SignatureException {

        if (contract != null && deadline != null && addrAlice != null) {
            PrivateKey privKey = ECCSignatureBuilder.getECCPrivateKey(privKeyFilepath);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.write(contract);
            baos.write(deadline);
            baos.write(addrAlice);
            nro = ECCSignatureBuilder.sign(baos.toByteArray(), privKey, DefaultConfiguration.SIGNATURE_ALGORITHM);
        } else {
            throw new RuntimeException("To generate NRO, contract, deadline and addrAlice have to be filled");
        }
    }

    public byte[] getBytes() throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException,
            SignatureException {

        if (contract != null && deadline != null && addrAlice != null && nro != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.write(contract);
            baos.write(deadline);
            baos.write(addrAlice);
            baos.write(nro);
            byte[] m1 = baos.toByteArray();
            return m1;
        } else {
            throw new RuntimeException("Contract, deadline, addrAlice and NRO have to be filled to generate M1");
        }
    }

}